import re
import sys
from requests import get
from bs4 import BeautifulSoup

BASE_URL = 'https://www.tfrrs.org/'
TEAM_URL = 'NJ_college_{}_New_Jersey_Institute'
PROFILE_M = re.compile(r'www\.tfrrs\.org\/athletes\/[0-9]+')

TRACK_S_M = re.compile(r'[0-9]+\.[0-9]+')
TRACK_M_M = re.compile(r'[0-9]+:[0-9]+\.[0-9]+')
TRACK_H_M = re.compile(r'[0-9]+:[0-9]+:[0-9]+\.[0-9]+')


def get_athlete(profile, g):
    result = {}
    soup = BeautifulSoup(
        ''.join(get('https://' + profile).text.split('\n')), 'lxml')
    sessions = tuple(soup.find(id='session-history').children)
    name = soup.find('h3').text
    result[name] = {}
    result[name]['tfrrs'] = profile
    result[name]['team'] = 'men' if g is 'm' else 'women'
    result[name]['seasons'] = {}
    for i in range(2, len(sessions) - 1, 2):
        result[name]['seasons'][sessions[i].string] = get_season_records(
            sessions[i + 1])
    return result


def get_season_records(table):
    result = {}
    for head in table.find_all('thead'):
        result[''.join(head.find('span').string.split('\t'))
               ] = find_season_record(head.parent)
    return result


def find_season_record(table):
    result = ''

    for row in table:
        if row.name == 'tr':
            mark = row.find('a').string
            result = mark if compare_marks(result, mark) else result

    return result


def compare_marks(prev, next):
    if prev is '' or None:
        return True
    elif next is None:
        return False
    elif 'm' in next or 'm' in prev:
        return parse_mark(prev) < parse_mark(next)
    else:
        return parse_time(prev) > parse_time(next)


def parse_mark(mark):
    if mark is None or mark == 'NH' or mark == 'FOUL' or mark == 'ND':
        return -sys.maxsize
    elif 'w' in mark or 'W' in mark:
        return float(mark[:-2])
    else:
        return float(mark[:-1])


def parse_time(time):
    if TRACK_S_M.match(time):
        if 'w' in time or 'W' in time or 'H' in time:
            return float(time[:-1])
        return float(time)
    elif TRACK_H_M.match(time):
        if 'w' in time or 'W' in time or 'H' in time:
            return int(time.split(':')[0]) * 60 * 60 + int(time.split(':')[1]) * 60 + float(time.split(':')[2][:-1])
        return int(time.split(':')[0]) * 60 * 60 + int(time.split(':')[1]) * 60 + float(time.split(':')[2])
    elif TRACK_M_M.match(time):
        if 'w' in time or 'W' in time or 'H' in time:
            return int(time.split(':')[1]) * 60 + float(time.split(':')[2][:-1])
        return int(time.split(':')[0]) * 60 + float(time.split(':')[1])
    return sys.maxsize


def get_roster_full():
    """Gets a list of both male and female athletes"""
    result = {}
    result.update(get_roster_men())
    result.update(get_roster_women())
    return result


def get_roster_men():
    """Gets a list of male athletes"""
    result = {}
    for profile in find_profiles(get_team_url('m')):
        result.update((get_athlete(profile, 'm')))
    result.update((get_athlete('www.tfrrs.org/athletes/6400855', 'm')))
    return result


def get_roster_women():
    """Gets a list of female athletes"""
    result = {}
    for profile in find_profiles(get_team_url('f')):
        result.update((get_athlete(profile, 'f')))
    return result


def get_team_url(g):
    """Returns a formatted url for a team based off gender"""
    return BASE_URL + 'teams/' + TEAM_URL.format(g)


def find_profiles(url):
    """Find all tfrrs profile links in a string"""
    return frozenset(PROFILE_M.findall(''.join(get(url).text.split())))
