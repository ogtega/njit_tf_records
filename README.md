# NJIT T&F/XC Records

This program generates an Excel spread sheet of each athlete's season best perfomance

### Getting Started
1. `git clone https://gitlab.com/ogtega/njit_tf_records.git records && cd records`
2. `python3 -m venv .env`
3. `source .env/bin/activate`
4. `pip install -r requirements.txt`
5. `python main.py`