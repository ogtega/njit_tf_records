#!./env/bin/ python3

import json
import tfrrs

from openpyxl import Workbook

seasons = dict()

athletes = tfrrs.get_roster_full()

for athlete in athletes:
    for season in athletes[athlete]['seasons']:
        seasons[season] = list()

for athlete in athletes:
    for season in seasons:
        for event in athletes[athlete]['seasons'].get(season, {}):
            events = set(seasons[season])
            events.add(event)
            seasons[season] = sorted(list(events))

wb = Workbook()

for season in seasons:
    events = seasons[season]
    ws = wb.create_sheet(season)
    ws.cell(row=1, column=1, value='Name')
    for i in range(len(events)):
        c = ws.cell(row=1, column=2 + i, value=events[i])

    names = sorted(athletes)
    for i in range(len(names)):
        name = names[i]
        athlete = athletes[name]
        c = ws.cell(row=2 + i, column=1, value=name)
        c.hyperlink = 'https://' + athlete['tfrrs']

        for j in range(len(events)):
            c = ws.cell(row=2 + i, column=2 + j,
                        value=athlete['seasons'].get(season, {}).get(events[j], ''))

wb.remove(wb[wb.sheetnames[0]])
wb.save('records.xlsx')
